//Client scripts:

import * as axios from "axios.js";
declare const window: any;

enum DocumentFormat {
  PDF = ".pdf",
  JPEG = ".jpeg",
}

enum DocumentLimit {
  BYTES = 2 * 10e5,
  MB = 2,
  QUANTITY = 3,
}

enum Currencies {
  USD = "usd",
  EUR = "eur",
  RUB = "rub",
  KZT = "kzt",
  UZS = "uzs",
}

const CURRENCY_NAMES = {
  [Currencies.USD]: "долларов",
  [Currencies.EUR]: "евро",
  [Currencies.RUB]: "рублей",
  [Currencies.KZT]: "тенге",
  [Currencies.UZS]: "сумов",
};

enum Operations {
  PURCHASING = "purchasing",
  SELLING = "selling",
}

const LIMITS = {
  [Operations.PURCHASING]: {
    MIN: 10e2,
    MAX: 10e11,
  },
  [Operations.SELLING]: {
    MIN: 10e-1,
    MAX: 10e3,
  },
};

const LIMITS_IN_WORDS = {
  [Operations.PURCHASING]: {
    MIN: "1 тысячи",
    MAX: "1 триллиона",
  },
  [Operations.SELLING]: {
    MIN: "1",
    MAX: "1 тысячи",
  },
};

const USD_LIMIT_WITHOUT_ID = 100;
const RATES_CACHE_VALIDITY_LIMIT = 15 * 60 * 60 * 1000; // 15 minutes
const BANK_ENDPOINT = "https://cbu.uz/ru/arkhiv-kursov-valyut/json/";
const RATES_KEY = "RATES_CACHE";
const NUMERATOR_KEY = "NUMERATOR_CACHE";
const NUMERATOR_STEP = 1;
const INITIAL_NUMERATOR = "1";
const CURATOR_GROUP_NAME = "Куратор кассы";
const DEFAULT_AMOUNT_TO = 0;
const DEFAULT_STRINGIFIED_JSON = "{}";
const EMPTY_NOTIFICATION = "";
const GENERATION_DATE_KEY = "GENERATION_DATE_CACHE";
const DATE_LOCALE = "ru";
const DATE_SEPARATOR = ".";
const DEFAULT_PAD = "000";

const MESSAGES = {
  API_NOT_AVAILABLE:
    "В настоящий момент сервис получения курса валют недоступен: ",
  ENTER_VALUE_MANUALLY: "Внесите сумму к выдаче вручную. ",
  OPERATION_RESTRICTED: "Невозможно произвести операцию. ",
  LAST_AVAILABLE_DATA: " Последние доступные данные от ",
  INCORRECT_AMOUNT_TO: "Некорректная сумма к выдаче",
  ALLOWED_DOCUMENT_FORMAT: `Допустимые форматы для загрузки - ${getObjectValues(
    DocumentFormat
  ).join(", ")}`,
  FILE_SIZE_LIMIT_EXCEEDED: `Максимальный размер загружаемого файла - ${DocumentLimit.MB} мегабайта.`,
  QUANTITY: `Загрузите не более ${DocumentLimit.QUANTITY} документов.`,
  AMOUNT_FROM_LIMIT: (from: string, to: string, currency: string) =>
    `Введите сумму от ${from} до ${to} ${currency}`,
};

type Rates = {
  [key in Currencies]: number;
};

const RATES_CACHE = {} as Rates;

async function retrieveRatesCache(): Promise<{
  timestamp: number;
  rates: Rates;
}> {
  const stringifiedCache =
    (await Application.storage.getItem(RATES_KEY)) || DEFAULT_STRINGIFIED_JSON;
  const { timestamp, rates } = JSON.parse(stringifiedCache);
  return { timestamp, rates };
}

async function getCurrencyRate(): Promise<void> {
  Context.data.restrictAmountToManualEntry = true;
  Context.data.error = "";
  Context.data.hasError = false;
  await axios
    .get(BANK_ENDPOINT)
    .then(async ({ data }: { data: { Ccy: string; Rate: string }[] }) => {
      data.forEach(({ Ccy: currency, Rate }) => {
        if (currency in Currencies) {
          RATES_CACHE[currency.toLowerCase() as Currencies] = parseFloat(Rate);
        }
      });
      await Application.storage.setItem(
        RATES_KEY,
        JSON.stringify({ timestamp: Date.now(), rates: RATES_CACHE })
      );
    })
    .catch(async (error: { message: string }) => {
      Context.data.error = MESSAGES.API_NOT_AVAILABLE + `${error.message}.`;
      Context.data.hasError = true;
      const isCurator = await isUserInCuratorGroup();
      if (isCurator) {
        Context.data.restrictAmountToManualEntry = false;
      }

      const { timestamp, rates } = await retrieveRatesCache();

      const now = Date.now();
      if (timestamp + RATES_CACHE_VALIDITY_LIMIT > now) {
        Context.data.error =
          (isCurator
            ? MESSAGES.ENTER_VALUE_MANUALLY
            : MESSAGES.OPERATION_RESTRICTED) + Context.data.error;
        return;
      }

      Context.data.error +=
        MESSAGES.LAST_AVAILABLE_DATA +
        `${new Date(timestamp).toLocaleString()}`;
      for (const currency in rates) {
        const key = currency.toLowerCase() as Currencies;
        RATES_CACHE[key] = rates[key];
      }
    });
}

function validateAmountFrom(): { isValid: boolean; notification: string } {
  const { amountFrom, exchangeType, foreignCurrency } = Context.data;
  if (
    amountFrom! >= LIMITS[exchangeType!.code].MIN &&
    amountFrom! <= LIMITS[exchangeType!.code].MAX
  ) {
    return { isValid: true, notification: EMPTY_NOTIFICATION };
  }

  const currentCurrency =
    exchangeType!.code === Operations.SELLING
      ? foreignCurrency!.code
      : Currencies.UZS;
  return {
    isValid: false,
    notification: MESSAGES.AMOUNT_FROM_LIMIT(
      LIMITS_IN_WORDS[exchangeType!.code].MIN,
      LIMITS_IN_WORDS[exchangeType!.code].MAX,
      CURRENCY_NAMES[currentCurrency]
    ),
  };
}

function validateAmountTo(): { isValid: boolean; notification: string } {
  const { amountTo } = Context.data;
  if (amountTo && amountTo > 0) {
    return { isValid: true, notification: EMPTY_NOTIFICATION };
  }

  return { isValid: false, notification: MESSAGES.INCORRECT_AMOUNT_TO };
}

async function validateIdDocument(): Promise<{
  isValid: boolean;
  notification: string;
}> {
  const { idDocument } = Context.data;
  if (!idDocument) return { isValid: true, notification: EMPTY_NOTIFICATION };
  if (idDocument!.length > DocumentLimit.QUANTITY)
    return {
      isValid: false,
      notification: MESSAGES.QUANTITY,
    };
  for (const document of idDocument) {
    const file = await document.fetch();
    const { __name, size } = file.data;
    if (
      !__name.endsWith(DocumentFormat.JPEG) &&
      !__name.endsWith(DocumentFormat.PDF)
    ) {
      return {
        isValid: false,
        notification: MESSAGES.ALLOWED_DOCUMENT_FORMAT,
      };
    }
    if (size! > DocumentLimit.BYTES) {
      return {
        isValid: false,
        notification: MESSAGES.FILE_SIZE_LIMIT_EXCEEDED,
      };
    }
  }
  return { isValid: true, notification: EMPTY_NOTIFICATION };
}

async function calculateAmountTo() {
  const { amountFrom, foreignCurrency, exchangeType } = Context.data;
  if (!foreignCurrency || !amountFrom || !exchangeType)
    return DEFAULT_AMOUNT_TO;
  const currency = foreignCurrency.code;
  if (!RATES_CACHE[currency]) getCurrencyRate();
  const rate = RATES_CACHE[currency];
  if (exchangeType.code === Operations.PURCHASING) return amountFrom / rate;
  if (exchangeType.code === Operations.SELLING) return amountFrom * rate;
}

async function shouldRequireID(): Promise<boolean> {
  const { amountFrom, amountTo, foreignCurrency, exchangeType, hasError } =
    Context.data;
  let ratesToCompare = RATES_CACHE;
  if (hasError) {
    const { rates } = await retrieveRatesCache();
    ratesToCompare = rates;
  }
  if (!amountFrom || !amountTo || !foreignCurrency || !exchangeType)
    return false;
  if (exchangeType.code === Operations.PURCHASING) {
    if (
      foreignCurrency.code === Currencies.USD ||
      foreignCurrency.code === Currencies.EUR
    ) {
      const rate =
        ratesToCompare[foreignCurrency.code] / ratesToCompare[Currencies.USD];
      return amountTo * rate > USD_LIMIT_WITHOUT_ID;
    }
    return amountTo > USD_LIMIT_WITHOUT_ID;
  } else {
    if (
      foreignCurrency.code === Currencies.USD ||
      foreignCurrency.code === Currencies.EUR
    ) {
      return amountFrom > USD_LIMIT_WITHOUT_ID;
    }
    const rate =
      ratesToCompare[foreignCurrency.code] / ratesToCompare[Currencies.USD];
    return amountFrom * rate > USD_LIMIT_WITHOUT_ID;
  }
}

function shouldRequirePinfl() {
  const { requireID, exchangeType } = Context.data;
  return requireID && exchangeType?.code === Operations.PURCHASING;
}

async function validateInput(): Promise<ValidationResult> {
  const result = new ValidationResult();
  const amountFromValidation = validateAmountFrom();
  if (!amountFromValidation.isValid) {
    result.addContextError("amountFrom", amountFromValidation.notification);
  }
  const amountToValidation = validateAmountTo();
  if (!amountToValidation.isValid) {
    result.addContextError("amountTo", amountToValidation.notification);
  }
  const documentValidation = await validateIdDocument();
  if (!documentValidation.isValid) {
    result.addContextError("idDocument", documentValidation.notification);
  }
  return result;
}

async function getNumerator(): Promise<string> {
  let numerator =
    (await Application.storage.getItem(NUMERATOR_KEY)) || INITIAL_NUMERATOR;
  const lastGenerationDate = await Application.storage.getItem(
    GENERATION_DATE_KEY
  );
  const newGenerationDate = new Date().toLocaleDateString();
  // if the previous document was generated yesterday,
  // reset numerator and set new generationDate
  if (lastGenerationDate !== newGenerationDate) {
    await Application.storage.setItem(GENERATION_DATE_KEY, newGenerationDate);
    numerator = INITIAL_NUMERATOR;
  }
  await Application.storage.setItem(
    NUMERATOR_KEY,
    String(parseInt(numerator) + NUMERATOR_STEP)
  );

  return numerator;
}

async function isUserInCuratorGroup(): Promise<boolean> {
  const user = await System.users.getCurrentUser();
  const group = await System.userGroups
    .search()
    .where((f) => f.__name.eq(CURATOR_GROUP_NAME))
    .first();
  const groupMembers = (await group?.users()) || [];
  return groupMembers.some((member) => member.id === user.id);
}

async function assignName() {
  const numerator = await getNumerator();
  const [day, month, year] = new Date()
    .toLocaleDateString(DATE_LOCALE)
    .split(DATE_SEPARATOR);
  const shortYear = year.slice(-2);
  // replacement of unsupported str.padStart()
  const index = (DEFAULT_PAD + numerator).slice(-4);
  Context.data.receiptName = `${day}\\${month}-${shortYear}-${index}`;
}

function assignExchangeLink() {
  const { host } = window.location;
  const {
    namespace,
    code: appID,
    data: { __id: id },
  } = Context;
  Context.data.exchangeLink = `${host}/(p:item/${namespace}/${appID}/${id})`;
}

function getObjectValues(object: { [key: string]: string }) {
  const accumulator: string[] = [];
  for (const key in object) {
    accumulator.push(object[key]);
  }
  return accumulator;
}

async function process(): Promise<void> {
  const {
    amountFrom,
    exchangeType,
    foreignCurrency,
    restrictAmountToManualEntry,
  } = Context.data;
  if (!amountFrom || !exchangeType?.code || !foreignCurrency?.code) return;

  const { isValid, notification } = validateAmountFrom();
  Context.data.isValidAmountFrom = isValid;
  Context.data.notification = notification;

  if (restrictAmountToManualEntry) {
    Context.data.amountTo = await calculateAmountTo();
  }

  Context.data.requireID = await shouldRequireID();
  Context.data.requirePinfl = shouldRequirePinfl();
  if (!Context.data.receiptName) assignName();
  if (!Context.data.exchangeLink) assignExchangeLink();
}




//Server scripts:

/* Server scripts module */
